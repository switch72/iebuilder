```

Robot Requirements:
Inventory Upgrade (required to store items in robot)
Inventory Controller Upgrade (required to get item info from items in inventory)


This lua program can be loaded onto a robot by opening an editor on a file called iebuilder and pasting the contents of iebuilder.lua
Or the program can be saved to pastebin and loaded onto a robot with an internet card by using the "pastebin get <pastebincode> iebuilder"
Then type iebuilder to run.

In order to build a structure, place the robot facing the direction you want the structure to be built away from you.
The structure will be built away from you and to the right.

The blocks to build the structure must be in the robot's inventory at the start of the construction.
The engineer's hammer must be in the robot's tool slot.

The program assumes there are blocks below the footprint of the machine to be built. 
If blocks do not exist, an angel upgrade will be necessary.

For pumpjack, deploy pipe option digs straight down from robot location to bedrock. Make sure robot is provided with sufficient pipe.
For ease of construction, construct pumpjack, then place robot at block below well head (below head of the pump) and run deploy pipe.

```
