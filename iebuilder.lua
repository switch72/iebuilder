
r = require("robot")
component = require("component")
--g = component.generator
inv = component.inventory_controller

machine_blocks = {
    ["pipe"] = {label = "Fluid Pipe", slot = {} , count = 0, required = 0},
    ["steelsheet"] = {label = "Steel Sheetmetal", slot = {} , count = 0, required = 0},
    ["HEblock"] = {label = "Heavy Engineering Block", slot = {} , count = 0, required = 0},
    ["steelscaff"] = {label = "Steel Scaffolding", slot = {} , count = 0, required = 0},
    ["LEblock"] = {label = "Light Engineering Block", slot = {} , count = 0, required = 0},
    ["REblock"] = {label = "Redstone Engineering Block", slot = {} , count = 0, required = 0},
    ["fence"] = {label = "Treated Wood Fence", slot = {} , count = 0, required = 0},
    ["steelblock"] = {label = "Block of Steel", slot = {} , count = 0, required = 0},
    ["radiator"] = {label = "Radiator Block", slot = {} , count = 0, required = 0},
    ["dirt"] = {label = "Dirt", slot = {} , count = 0, required = 0}
}


pumpjackBp = {
    {
        {"steelscaff","steelscaff","steelscaff","pipe","steelscaff"},
        {"steelscaff","steelscaff","steelscaff","pipe","pipe","steelscaff"},
        {"LEblock","steelscaff","steelscaff","pipe","steelscaff"}
    },
    {
        {"REblock","air","temp","fence"},
        {"air","HEblock"},
        {"LEblock","air","temp","fence"}
    },
    {
        {"air","air","air","fence"},
        {"air","HEblock","temp","temp","temp","steelblock"},
        {"air","air","air","fence"}                           
    },
    {
        {"air","air","air","fence"},
        {"air","steelsheet","steelsheet","steelsheet","steelsheet","steelblock"},
        {"air","air","air","fence"}       
    }   
}

excavatorBp = {
    {
        {"air"},
        {"air","air","air","air","steelblock"}
    },
    {
        {"dirt"},
        {"air","air","steelblock","steelscaff","steelscaff","steelscaff","steelblock"}
    },
    {
        {"radiator","steelsheet","steelsheet","steelscaff","steelscaff","steelscaff"},
        {"steelsheet","air","steelscaff","steelscaff","steelscaff","steelscaff","steelscaff"},
        {"steelsheet","LEblock","steelsheet","steelscaff","steelscaff","steelscaff"}
    },
    {
        {"REblock","steelsheet","steelsheet","LEblock","air","LEblock"},
        {"HEblock","steelblock","steelscaff","steelscaff","steelblock","steelscaff","steelscaff","steelblock"},
        {"steelsheet","LEblock","steelsheet","HEblock","HEblock","HEblock"}
    },
    {
        {"HEblock","radiator","radiator","LEblock","LEblock","LEblock"},
        {"steelsheet","air","steelscaff","steelscaff","steelscaff","steelscaff","steelscaff"},
        {"steelsheet","LEblock","steelsheet","steelsheet","steelsheet","steelsheet"}
    },
    {
        {"air"},
        {"air","air","steelblock","steelscaff","steelscaff","steelscaff","steelblock"}
    },
    {
        {"air"},
        {"air","air","air","air","steelblock"}
    }
}

function findBedrock()
    local moved = 0
    while true do
        if r.detectDown() then
            if not r.swingDown() then
                print "Found Bedrock"
                break
            end
        else
            r.down()
            moved = moved + 1
        end
    end
    return moved
end

function checkinventory()
    sufficientMaterials = true
    
    for slot = 1, r.inventorySize(), 1 do
        r.select(slot)
        if r.count() > 0 then
            for block, blockinfo in pairs(machine_blocks) do
                if inv.getStackInInternalSlot().label == blockinfo.label then
                    table.insert(blockinfo.slot,slot)
                    blockinfo.count = blockinfo.count + inv.getStackInInternalSlot().size
                    break
                end
            end
        end
    end
    for block, blockinfo in pairs(machine_blocks) do
        if blockinfo.count < blockinfo.required then
            sufficientMaterials = false
            print ("Insufficient \"" .. blockinfo.label .. "\" need (" .. blockinfo.required - blockinfo.count .. ") additional blocks.")
        end
    end

    return sufficientMaterials
end
        

function deploypipe()
    moved = findBedrock()
    r.select(machine_blocks.pipe.slot)
    outofpipe = 0
    pipeplaced = 0
    for i=1, moved, 1 do
        r.up()
        if inv.getStackInInternalSlot().size > 0 then
            r.placeDown()
            pipeplaced = pipeplaced + 1
        else
            outofpipe = 1
        end
    end

    if outofpipe == 1 then
        print ("Ran out of pipe")
        print ("Require " .. pipeplaced - moved .. " additional pipe.")
    end
    print ("Used " .. pipeplaced .. " pipe.")
end

function build(blueprint)
    x = 0
    y = 0
    z = 0
    tempx = {}
    for layer, layertable in ipairs(blueprint) do
        r.swingUp()
        r.up()
        y = y + 1
        for row, rowtable in ipairs (layertable) do
            for index, block in ipairs (rowtable) do
                r.swing()
                r.forward()
                r.swingDown()
                x = x + 1
                if block == "air" then 
                else
                    if block == "temp" then
                        tempx[x] = true
                        block = "dirt"
                    end
                    r.select(machine_blocks[block].slot[1])
                    if r.count() < 1 then --check if out of this block type in this slot
                        table.remove(machine_blocks[block].slot,1) --remove this slot from list of slots with this block type
                        if machine_blocks[block].slot[1] then
                        r.select(machine_blocks[block].slot[1]) --change to next slot of this block type
                        else
                            print("Error, ran out of materials")
                            return false
                        end
                    end
                    r.placeDown()
                    
                end
            end
            for start = x, 1 , -1 do
                if tempx[x] then
                    r.swingDown()
                    tempx[x]=nil
                end
                r.back()
                x = x - 1
            end
            if next(layertable,row) then --check if there are more rows in this layer
                r.turnRight()
                r.swing()
                r.forward()
                z = z + 1
                r.turnLeft()
            end
        end
        r.turnRight()
        for start = z, 1 , -1 do
            r.back()
            z = z - 1
        end
        r.turnLeft()
    end
    for start = y, 1 , -1 do
        r.swingDown()
        r.down()
        y = y - 1
    end
    return true
end

function deconstruct(blueprint)
    x = 0
    y = 0
    z = 0
    for layer, layertable in ipairs(blueprint) do
        r.up()
        y = y + 1
        for row, rowtable in ipairs (layertable) do
            for index, block in ipairs (rowtable) do
                r.swing()
                r.forward()
                r.swingDown()
                x = x + 1
            end
            for start = x, 1 , -1 do
                r.back()
                x = x - 1
            end
            if next(layertable,row) then --check if there are more rows in this layer
                r.turnRight()
                r.swing()
                r.forward()
                z = z + 1
                r.turnLeft()
            end
        end
        r.turnRight()
        for start = z, 1 , -1 do
            r.back()
            z = z - 1
        end
        r.turnLeft()
    end
    for start = y, 1 , -1 do
        r.down()
        y = y - 1
    end
    print("X="..x.."Y="..y.."Z="..z)
end



function buildpumpjack()
    machine_blocks.pipe.required = 4
    machine_blocks.steelsheet.required = 4
    machine_blocks.HEblock.required = 2
    machine_blocks.steelscaff.required = 11
    machine_blocks.LEblock.required = 2
    machine_blocks.REblock.required = 1
    machine_blocks.fence.required = 6
    machine_blocks.steelblock.required = 2
    machine_blocks.dirt.required = 3
    if checkinventory() then 
        print ("There is sufficient inventory to build the pumpjack.")


        build(pumpjackBp)

        r.up()
        r.turnRight()
        r.forward()
        r.turnLeft()
        r.forward()
        r.use()
        r.back()
        r.turnRight()
        r.back()
        r.turnLeft()
        r.down()


    else
        print ("There are not enough materials to build the pumpjack.")
    end

end

function buildexcavator()
    machine_blocks.pipe.required = 0
    machine_blocks.steelsheet.required = 15
    machine_blocks.HEblock.required = 5
    machine_blocks.steelscaff.required = 26
    machine_blocks.LEblock.required = 9
    machine_blocks.REblock.required = 1
    machine_blocks.fence.required = 0
    machine_blocks.radiator.required = 3
    machine_blocks.steelblock.required = 9
    machine_blocks.dirt.required = 1
   if  checkinventory() then
    r.swingDown()
    r.down()
    r.swingDown()
    r.down()
    build(excavatorBp)
    r.up()
    r.up()

    r.turnLeft()
    r.swing()
    r.forward()
    r.turnRight()
    r.up()
    r.forward()
    r.forward()
    r.forward()
    r.forward()
    r.forward()
    r.turnRight()
    r.forward()
    r.use()
    r.back()
    r.select(machine_blocks.LEblock.slot[1])
    r.place()
    r.back()
    r.turnLeft()
    r.back()
    r.back()
    r.back()
    r.back()
    r.back()
    r.down()
    r.down()
    r.turnRight()
    r.forward()
    r.forward()
    r.forward()
    r.turnLeft()
    r.use()

   end


end

function main()
    runmenu = true

    while runmenu do
        print("Select BLueprint:")
        print("1. Pumpjack")
        print("2. Excavator")
        print("0. Exit")
        selectedbp = math.floor(math.abs(tonumber(io.read())))
        if selectedbp == 1 then
            print("Select Action:")
            print("1. Construct")
            print("2. Deconstruct")
            print("3. Deploy Pipe to Bedrock")
            print("0. Return")
            selectedaction = math.floor(math.abs(tonumber(io.read())))
            if selectedaction == 1 then
                buildpumpjack()
            elseif selectedaction == 2 then
                deconstruct(pumpjackBp)
            elseif selectedaction == 3 then
                deploypipe()
            end
        elseif selectedbp == 2 then
            print("Select Action:")
            print("1. Construct")
            print("2. Deconstruct")
            print("0. Return")
            selectedaction = math.floor(math.abs(tonumber(io.read())))
            if selectedaction == 1 then
                buildexcavator()
            elseif selectedaction == 2 then
                r.swingDown()
                r.down()
                r.swingDown()
                r.down()
                deconstruct(excavatorBp)
                r.up()
                r.up()
            end
        elseif selectedbp == 0 then
            runmenu = false
        else
            print("Invalid selection, enter the number of your choice.")
        end
    end
end

main()